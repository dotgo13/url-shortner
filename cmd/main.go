package main

import (
	_ "github.com/joho/godotenv/autoload"
	"gitlab.com/dotgo13/url-shortner/pkg/config"
	"gitlab.com/dotgo13/url-shortner/repository/postgres"
	"gitlab.com/dotgo13/url-shortner/repository/redis"
	"gitlab.com/dotgo13/url-shortner/server/api/controllers"
	"gitlab.com/dotgo13/url-shortner/server/api/router"
	_ "gitlab.com/dotgo13/url-shortner/server/docs"
	"gitlab.com/dotgo13/url-shortner/service"
)

// @title API
// @version 1
// @description This is an auto-generated API Docs.
// @termsOfService http://swagger.io/terms/
// @contact.name Akbarshoh Ismoilov
// @contact.email akbarshohismoilov001@gmail.com
// @BasePath /
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	cfg := config.NewConfig()
	repo := postgres.NewPostgres(*cfg)
	sv := service.NewService(repo)
	r := redis.NewRedisRepo(*cfg)
	s := controllers.NewServer(sv, r)
	router.Run(s, *cfg)
}
