package router

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.com/dotgo13/url-shortner/pkg/config"
	"gitlab.com/dotgo13/url-shortner/pkg/middleware"
	"gitlab.com/dotgo13/url-shortner/server/api/controllers"
)

func routes(sv *controllers.Server, r *gin.Engine) {
	r.GET("/get-qr-code/", sv.GetQRCode)
	r.POST("/register/", sv.Register)
	r.POST("/login/", sv.Login)
	r.POST("/new-short-url/", sv.NewShortURL)
	r.GET("/get-long/", sv.GetLongURL)
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}

func Run(sv *controllers.Server, cfg config.Config) {

	r := gin.Default()
	r.Use(middleware.Authorizer(cfg))
	r.Use(cors.Default())
	routes(sv, r)
	if err := r.Run(fmt.Sprintf("%s:%d", cfg.Host, cfg.Port)); err != nil {
		fmt.Println("router not running, ", err)
	}
}
