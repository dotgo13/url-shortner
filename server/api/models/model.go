package models

type SignUpRequestModel struct {
	UserID   string `json:"user_id"`
	FullName string `json:"full_name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type SignUpResponseModel struct {
	UserID       string `json:"user_id"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type LoginRequestModel struct {
	Password string `json:"password"`
}

type NewShortURLRequest struct {
	LongURL  string `json:"long_url"`
	ShortURL string `json:"short_url"`
	Clicked  uint64 `json:"clicked"`
	ExpDate  uint64 `json:"exp_date"`
}

type NewShortURLResponse struct {
	ShortURL   string `json:"short_url"`
	ShortToken string `json:"short_token"`
}
