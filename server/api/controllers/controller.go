package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/dotgo13/url-shortner/pkg/etc"
	"gitlab.com/dotgo13/url-shortner/pkg/jwt"
	"gitlab.com/dotgo13/url-shortner/server/api/models"
	"net/http"
	"strings"
	time2 "time"
)

// Register signup
// @Summary      registers an user
// @Description  register with password and email
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        signupModel   body      models.SignUpRequestModel  true  "signup body"
// @Success      200  {object}  map[string]string
// @Failure      400  {object}  map[string]string
// @Failure      500  {object}  map[string]string
// @Router       /register [post]
func (s *Server) Register(c *gin.Context) {
	var (
		body models.SignUpRequestModel
	)
	if err := c.ShouldBindJSON(&body); err != nil {
		c.JSON(http.StatusBadRequest, map[string]string{
			"error": "cannot parse body, " + err.Error(),
		})
		return
	}

	body.Email = strings.TrimSpace(body.Email)
	body.Email = strings.ToLower(body.Email)
	id := uuid.New()

	// creating access and refresh tokens
	tokens, err := jwt.GenerateNewTokens(id.String(), map[string]string{"role": "user"})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]string{
			"error": "cannot generate tokens, " + err.Error(),
		})
		return
	}
	accessToken, refreshToken := tokens.Access, tokens.Refresh
	password, err := etc.GeneratePasswordHash(body.Password)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]string{
			"error": "cannot generate password, " + err.Error(),
		})
		return
	}

	if err := s.sv.Register(models.SignUpRequestModel{
		UserID:   id.String(),
		FullName: body.FullName,
		Email:    body.Email,
		Password: string(password),
	}); err != nil {
		c.JSON(http.StatusInternalServerError, map[string]string{
			"error": "cannot create user, " + err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, models.SignUpResponseModel{
		UserID:       id.String(),
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	})
}

// Login log
// @Summary      login to account
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        loginModel   body      models.LoginRequestModel  true  "login body"
// @Success      200  {object}  map[string]string
// @Failure      400  {object}  map[string]string
// @Failure      500  {object}  map[string]string
// @Router       /login [post]
func (s *Server) Login(c *gin.Context) {
	var (
		body models.LoginRequestModel
	)
	user, err := jwt.ExtractTokenMetadata(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]string{
			"error": "cannot parse body, " + err.Error(),
		})
		return
	}
	if err := c.ShouldBindJSON(&body); err != nil {
		c.JSON(http.StatusBadRequest, map[string]string{
			"error": "cannot parse body, " + err.Error(),
		})
		return
	}

	if err := s.sv.Login(body, user.UserID.String()); err != nil {
		c.JSON(http.StatusBadRequest, map[string]string{
			"error": "cannot login, " + err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, map[string]string{
		"success": "true",
	})
}

func (s *Server) GetQRCode(c *gin.Context) {
	url := c.Query("url")
	fmt.Println(url)
	path, _ := s.sv.GetQRCode(url)
	c.File(path)
}

// NewShortURL log
// @Summary      New Short URL
// @Tags         url
// @Security ApiKeyAuth
// @Accept       json
// @Produce      json
// @Param        loginModel   body      models.NewShortURLRequest  true  "login body"
// @Success      200  {object}  models.NewShortURLResponse
// @Failure      400  {object}  map[string]string
// @Failure      500  {object}  map[string]string
// @Router       /new-short-url/ [post]
func (s *Server) NewShortURL(c *gin.Context) {
	var (
		body models.NewShortURLRequest
	)
	if err := c.ShouldBindJSON(&body); err != nil {
		c.JSON(http.StatusBadRequest, map[string]string{
			"error": "cannot parse body, " + err.Error(),
		})
		return
	}

	user, err := jwt.ExtractTokenMetadata(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]string{
			"error": "cannot parse body, " + err.Error(),
		})
		return
	}

	res, err := s.sv.NewShortURL(body, user.UserID.String())
	if err != nil {
		c.JSON(500, map[string]string{
			"error": "server:" + err.Error()})
		return
	}
	c.JSON(http.StatusOK, res)
}

// GetLongURL log
// @Summary      get long url
// @Tags         url
// @Security ApiKeyAuth
// @Accept       json
// @Produce      json
// @Param        short_url   query    string  true  "get long url"
// @Success      200  {object}  string
// @Failure      400  {object}  map[string]string
// @Failure      500  {object}  map[string]string
// @Router       /get-long/{url} [get]
func (s *Server) GetLongURL(c *gin.Context) {
	url := c.Query("url")
	long, err := s.r.Get(url)
	if err != nil || long == nil {
		res, err := s.sv.GetLongURL(url)
		if err != nil {
			c.JSON(http.StatusBadRequest, map[string]string{
				"error": "cannot get long url, " + err.Error(),
			})
			return
		}
		if err := s.r.SetWithTTL(url, res.Long, int(time2.Second.Milliseconds()/1000)*10); err != nil {
			c.JSON(http.StatusInternalServerError, map[string]string{
				"error": "cannot set time to redis, " + err.Error(),
			})
			return
		}
		c.JSON(http.StatusOK, res.Long)
		return
	}
	if err := s.sv.ClickURL(url); err != nil {
		c.JSON(http.StatusInternalServerError, map[string]string{
			"error": "cannot track click, " + err.Error(),
		})
		return
	}
	//log.Println(string(long.([]uint8))) // OUTPUT 'long' edi
	c.JSON(http.StatusOK, string(long.([]uint8)))
	return
}
