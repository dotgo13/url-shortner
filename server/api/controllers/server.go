package controllers

import (
	"gitlab.com/dotgo13/url-shortner/repository/repo"
	"gitlab.com/dotgo13/url-shortner/service"
)

type Server struct {
	sv service.IService
	r  repo.InMemoryStorageI
}

func NewServer(sv service.IService, r repo.InMemoryStorageI) *Server {
	return &Server{
		sv: sv,
		r:  r,
	}
}
