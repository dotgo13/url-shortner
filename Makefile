APP=url-shortener
CURRENT_DIR=$(shell pwd)

ifneq (,$(wildcard ./.env))
	include .env
endif

make create-env:
	cp ./.env.sample ./.env

run:
	go run cmd/main.go

lint: ## Run lint test
	golint -set_exit_status ${PKG_LIST}

swag-gen:
	swag init -g cmd/main.go -o server/docs

unit-tests: ## Run unit-tests
	go test -mod=vendor -v -cover -short ./...

build:
	go build -o ${APP} cmd/main.go
