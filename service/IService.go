package service

import (
	"gitlab.com/dotgo13/url-shortner/repository"
	"gitlab.com/dotgo13/url-shortner/server/api/models"
)

type IService interface {
	GetQRCode(url string) (string, error)
	NewShortURL(req models.NewShortURLRequest, userID string) (models.NewShortURLResponse, error)
	Register(req models.SignUpRequestModel) error
	Login(req models.LoginRequestModel, userID string) error
	ClickURL(url string) error
	GetLongURL(shortURL string) (repository.GetLongURLResponse, error)
}
