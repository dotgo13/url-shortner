package service

import (
	"gitlab.com/dotgo13/url-shortner/repository"
	"gitlab.com/dotgo13/url-shortner/repository/repo"
	"gitlab.com/dotgo13/url-shortner/server/api/models"
)

type service struct {
	r repo.Repository
}

func NewService(repo repo.Repository) *service {
	return &service{
		r: repo,
	}
}

func (s *service) GetQRCode(url string) (string, error) {
	return s.r.GetQRCode(url)
}

func (s *service) NewShortURL(req models.NewShortURLRequest, userID string) (models.NewShortURLResponse, error) {
	return s.r.NewShortURL(req, userID)
}

func (s *service) Register(req models.SignUpRequestModel) error {
	return s.r.Register(req)
}

func (s *service) Login(req models.LoginRequestModel, userID string) error {
	return s.r.Login(req, userID)
}

func (s *service) ClickURL(url string) error {
	return s.r.ClickURL(url)
}

func (s *service) GetLongURL(shortURL string) (repository.GetLongURLResponse, error) {
	return s.r.GetLongURL(shortURL)
}
