FROM golang:1.20 as builder
LABEL maintainer=".go team"

RUN mkdir ~/app

ENV APP=url-shortener

WORKDIR ~/app

COPY . .

RUN apt-get update  \
    && apt-get install make \
    && make build \
    && mv ${APP} /bin/

ENTRYPOINT ["url-shortener"]

EXPOSE 8000