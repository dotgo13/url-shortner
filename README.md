# url-shortner

## Name
    url-shortener

## Description
url-shortener is a service for making your urls shorter and use given urls for your project

## Installation
You should install docker and docker compose if not exists

## Usage
    docker build -t url-shortener .
    docker compose up -d

## Support
    https://t.me/software_engineer_001 ## telegram

    akbarshohismoilov001@gmail.com ## mail

## Authors and acknowledgment
.go team has hold authorization licence

## Project status
Completed