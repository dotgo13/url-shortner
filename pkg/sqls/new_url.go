package sqls

const (
	SelectLongURL = `
		SELECT 
			id,
			user_id
		FROM long_url 
		WHERE url=$1;
    `
	SelectShortURL = `
    	SELECT id FROM short_url WHERE url = $1
	`
	InsertLongURL = `
		INSERT INTO long_url (id, url, user_id) VALUES ($1,$2,$3)
	`
	InsertShortURL = `
		INSERT INTO short_url (id, long_url_id, url, click_num, exp_date) VALUES ($1,$2,$3,$4,$5)
	`

	SelectImageID = `
		SELECT image_id FROM short_url WHERE id=$1
	`
)
