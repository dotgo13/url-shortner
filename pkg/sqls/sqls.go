package sqls

var (
	Register = `
		INSERT INTO users
		VALUES($1, $2, $3, $4)
	`

	Login = `
		SELECT password 
		FROM users
		WHERE id=$1
	`

	ClickURL = `
		UPDATE short_url 
		SET clicked = clicked + 1
		WHERE url = $1
		`

	GetLongURL = `
		SELECT l.url, s.exp_date, s.click_num - s.clicked
		FROM long_url l 
		JOIN short_url s 
		ON l.id = s.long_url_id
		WHERE s.url = $1
		`
)
