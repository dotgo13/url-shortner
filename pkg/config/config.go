package config

import (
	"github.com/spf13/cast"
	"os"
)

type Config struct {
	Port int
	Host string

	Environment      string
	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string

	CasbinConfigPath    string
	MiddlewareRolesPath string
	SigningKey          string
	JWTSecretKey        string

	JWTSecretKeyExpireMinutes int
	JWTRefreshKey             string
	JWTRefreshKeyExpireHours  int

	RedisHost string
	RedisPort int
}

func NewConfig() *Config {
	return &Config{
		Environment:               cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop")),
		Port:                      cast.ToInt(getOrReturnDefault("PORT", 8000)),
		Host:                      cast.ToString(getOrReturnDefault("HOST", "localhost")),
		PostgresHost:              cast.ToString(getOrReturnDefault("POSTGRES_HOST", "postgres")),
		PostgresPort:              cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5432)),
		PostgresDatabase:          cast.ToString(getOrReturnDefault("POSTGRES_DB", "url")),
		PostgresUser:              cast.ToString(getOrReturnDefault("POSTGRES_USER", "postgres")),
		PostgresPassword:          cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "")),
		CasbinConfigPath:          cast.ToString(getOrReturnDefault("CASBIN_CONFIG_PATH", "./pkg/config/rbac_model.conf")),
		MiddlewareRolesPath:       cast.ToString(getOrReturnDefault("MID_ROLES_PATH", "./pkg/config/models.csv")),
		SigningKey:                cast.ToString(getOrReturnDefault("SIGNING_KEY", "")),    //secret
		JWTSecretKey:              cast.ToString(getOrReturnDefault("JWT_SECRET_KEY", "")), // secret
		JWTSecretKeyExpireMinutes: cast.ToInt(getOrReturnDefault("JWT_EXPIRE_MINUTE", 120)),
		JWTRefreshKey:             cast.ToString(getOrReturnDefault("JWT_REFRESH_KEY", "")), // secret
		JWTRefreshKeyExpireHours:  cast.ToInt(getOrReturnDefault("JWT_REFRESH_EXPIRE", 24)),
		RedisPort:                 cast.ToInt(getOrReturnDefault("REDIS_PORT", 6376)),
		RedisHost:                 cast.ToString(getOrReturnDefault("REDIS_HOST", "localhost")),
	}
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
