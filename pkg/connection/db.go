package connection

import (
	"errors"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dotgo13/url-shortner/pkg/config"
	"log"
)

func NewDB(config config.Config) *sqlx.DB {
	url, err := NewConnection("postgres", config)
	if err != nil {
		log.Fatal("error in creating postgres url, ", err.Error())
	}
	db, err := sqlx.Connect("postgres", url)
	if err != nil {
		log.Fatal("error in connecting to postgres, ", err.Error())
	}

	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	println(driver)
	m, err := migrate.NewWithDatabaseInstance(
		"file://pkg/migrations",
		"postgres", driver,
	)
	if err = m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {

		log.Fatal("failed to migrate, ", err.Error())
	}

	return db
}
