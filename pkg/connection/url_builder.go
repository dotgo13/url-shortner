package connection

import (
	"fmt"
	"gitlab.com/dotgo13/url-shortner/pkg/config"
)

func NewConnection(t string, config config.Config) (string, error) {
	var url string
	switch t {
	case "postgres":
		// url for postgres connection to database
		url = fmt.Sprintf(
			"postgres://%s:%s@%s:%d/%s?sslmode=disable",
			config.PostgresUser,
			config.PostgresPassword,
			config.PostgresHost,
			config.PostgresPort,
			config.PostgresDatabase,
		)
	case "migration":
		// url for migration
		url = fmt.Sprintf(
			"postgres://%s:%s@%s:%d/%s?sslmode=disable",
			config.PostgresUser,
			config.PostgresPassword,
			config.PostgresHost,
			config.PostgresPort,
			config.PostgresDatabase,
		)
	default:
		// Return error message.
		return "", fmt.Errorf("connection name '%v' is not supported", t)
	}

	return url, nil
}
