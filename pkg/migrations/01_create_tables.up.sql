CREATE TABLE users(
    id uuid PRIMARY KEY ,
    full_name TEXT NOT NULL ,
    email TEXT NOT NULL ,
    password TEXT NOT NULL
);

CREATE TABLE long_url(
    id uuid PRIMARY KEY ,
    url TEXT NOT NULL UNIQUE ,
    user_id uuid REFERENCES users(id)
);

CREATE TABLE short_url(
    id uuid PRIMARY KEY ,
    long_url_id uuid REFERENCES long_url(id) ,
    url TEXT NOT NULL UNIQUE ,
    clicked INTEGER DEFAULT 0 ,
    click_num INTEGER NOT NULL ,
    created_at TIME DEFAULT CURRENT_TIME ,
    exp_date TIME NOT NULL
);