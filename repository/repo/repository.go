package repo

import (
	"gitlab.com/dotgo13/url-shortner/repository"
	"gitlab.com/dotgo13/url-shortner/server/api/models"
)

type Repository interface {
	Get
	Set
}

type Get interface {
	NewShortURL(req models.NewShortURLRequest, userID string) (models.NewShortURLResponse, error) // Soburjon
	GetLongURL(shortURL string) (repository.GetLongURLResponse, error)
	//ListUserURL()      // Abdurahmon
	//ListShortURL()     // Abdurahmon
	//GetClickShortURL() // Abdurahmon
	GetQRCode(url string) (string, error) // Soburjon
	//GetProfile()       // Abdurahmon
}

type Set interface {
	Register(body models.SignUpRequestModel) error
	Login(body models.LoginRequestModel, userID string) error
	//UpdateShortURL() // Soburjon
	//DeleteShortURL() // Abdurahmon
	ClickURL(urlID string) error
}
