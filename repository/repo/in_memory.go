package repo

// InMemoryStorageI is interface for storage in memory
type InMemoryStorageI interface {
	SetWithTTL(key, value string, seconds int) error
	Get(key string) (interface{}, error)
}
