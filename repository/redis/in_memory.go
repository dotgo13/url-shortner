package redis

import (
	rd "github.com/gomodule/redigo/redis"
	"gitlab.com/dotgo13/url-shortner/pkg/config"
)

type redisRepo struct {
	rds *rd.Pool
}

// NewRedisRepo ...
func NewRedisRepo(config config.Config) *redisRepo {
	rds := redisPool(config)
	return &redisRepo{rds: rds}
}

// SetWithTTL ...
func (r *redisRepo) SetWithTTL(key, value string, seconds int) (err error) {
	conn := r.rds.Get()
	defer conn.Close()

	_, err = conn.Do("SETEX", key, seconds, value)
	return
}

func (r *redisRepo) Get(key string) (interface{}, error) {
	conn := r.rds.Get()
	defer conn.Close()

	return conn.Do("GET", key)
}
