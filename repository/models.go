package repository

type Login struct {
	Password string `db:"password"`
}

type GetLongURLResponse struct {
	Long       string
	Time       string
	CanClicked int
}
