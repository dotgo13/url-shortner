package postgres

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dotgo13/url-shortner/pkg/config"
	"gitlab.com/dotgo13/url-shortner/pkg/connection"
	"gitlab.com/dotgo13/url-shortner/pkg/sqls"
	"gitlab.com/dotgo13/url-shortner/pkg/utils"
	"gitlab.com/dotgo13/url-shortner/repository"
	"gitlab.com/dotgo13/url-shortner/server/api/models"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

type postgres struct {
	db *sqlx.DB
}

func NewPostgres(config config.Config) *postgres {
	return &postgres{
		db: connection.NewDB(config),
	}
}

func (p *postgres) Register(body models.SignUpRequestModel) error {
	if _, err := p.db.Exec(sqls.Register, body.UserID, body.FullName, body.Email, body.Password); err != nil {
		return err
	}

	return nil
}

func (p *postgres) Login(body models.LoginRequestModel, userID string) error {
	var b string
	if err := p.db.QueryRow(sqls.Login, userID).Scan(&b); err != nil {
		return err
	}
	err := bcrypt.CompareHashAndPassword([]byte(b), []byte(body.Password))
	if err != nil {
		return err
	}

	return nil
}

func (p *postgres) GetQRCode(url string) (string, error) {
	url1 := fmt.Sprintf("https://api.qrserver.com/v1/create-qr-code/?data=%s&size=1000x1000", url)
	method := "GET"

	payload := &bytes.Buffer{}

	client := &http.Client{}
	req, err := http.NewRequest(method, url1, payload)

	if err != nil {
		fmt.Println("222: ", err)
		return "", err
	}
	//req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	name := make([]byte, 0)
	res.Body.Read(name)
	fmt.Println(name)

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("444: ", err)
		return "", err
	}
	err = os.WriteFile("server/images/salom.png", body, 0644)
	if err != nil {
		return "", err
	}
	return "server/images/salom.png", nil
}

func (p *postgres) NewShortURL(req models.NewShortURLRequest, userID string) (models.NewShortURLResponse, error) {
	var res = models.NewShortURLResponse{}
	var (
		URLID, URLUserID string
	)
	URLID = uuid.NewString()
	err := p.db.QueryRow(sqls.SelectLongURL, req.LongURL).Scan(&URLID, &URLUserID)
	if errors.Is(err, sql.ErrNoRows) {
		if _, err = p.db.Exec(sqls.InsertLongURL, URLID, req.LongURL, userID); err != nil {
			return models.NewShortURLResponse{}, err
		}
	} else if err != nil {
		return models.NewShortURLResponse{}, err
	}
	ShortURLID := uuid.NewString()
	err = p.db.QueryRow(sqls.SelectShortURL, req.ShortURL).Scan(&ShortURLID)
	if err == nil {
		return models.NewShortURLResponse{}, errors.New("bu bor")
	} else if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return models.NewShortURLResponse{}, err
	}
	var (
		imageID  int
		ShortURL string
	)

	var Time time.Time
	if req.ShortURL != "" {
		ShortURL = req.ShortURL
	} else {
		ShortURL = utils.String(10)
	}
	for req.ExpDate != 0 {
		Time = time.Now().Add(time.Hour * 24)
		req.ExpDate -= 1
	}

	_, err = p.db.Exec(sqls.InsertShortURL, ShortURLID, URLID, ShortURL, req.Clicked, Time)
	if err != nil {
		return models.NewShortURLResponse{}, err
	}

	if err := p.db.QueryRow(sqls.SelectImageID, ShortURLID).Scan(&imageID); err != nil {
		return models.NewShortURLResponse{}, err
	}

	url1 := fmt.Sprintf("https://api.qrserver.com/v1/create-qr-code/?data=%s&size=1000x1000", ShortURL)
	method := "GET"

	payload := &bytes.Buffer{}

	client := &http.Client{}
	req1, err := http.NewRequest(method, url1, payload)

	if err != nil {
		return models.NewShortURLResponse{}, err
	}
	res1, err := client.Do(req1)
	if err != nil {
		return models.NewShortURLResponse{}, err
	}

	defer res1.Body.Close()

	body, err := ioutil.ReadAll(res1.Body)
	if err != nil {
		return models.NewShortURLResponse{}, err
	}
	err = os.WriteFile(fmt.Sprintf("server/images/%d.png", imageID), body, 0644)
	if err != nil {
		return models.NewShortURLResponse{}, err
	}
	fmt.Println(ShortURL)
	res.ShortURL = fmt.Sprintf("localhost:8080/click-url/?url=%s", ShortURL)
	res.ShortToken = ShortURL
	return res, nil
}

func (p *postgres) ClickURL(url string) error {
	if _, err := p.db.Exec(sqls.ClickURL, url); err != nil {
		return err
	}

	return nil
}

func (p *postgres) GetLongURL(shortURL string) (repository.GetLongURLResponse, error) {
	var (
		url      string
		exp      string
		canClick int
	)

	if err := p.ClickURL(shortURL); err != nil {
		return repository.GetLongURLResponse{}, err
	}
	if err := p.db.QueryRow(sqls.GetLongURL, shortURL).Scan(&url, &exp, &canClick); err != nil {
		return repository.GetLongURLResponse{}, err
	}

	return repository.GetLongURLResponse{
		Long:       url,
		Time:       exp,
		CanClicked: canClick,
	}, nil
}
